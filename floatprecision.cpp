// Floating point precision calculator
// by Eugene Hopkinson aka SlowRiot 2013
//
// compile with:
// g++ -std=gnu++1y floatprecision.cpp -o floatprecision

#include <iostream>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <limits>

int main(int argc, char *argv[]) {
  if(argc != 2) {
    std::cerr << "Usage: " << argv[0] << " <floating point value>" << std::endl;
    return EXIT_FAILURE;
  }

  // get the end pointer for the argument
  //const char *endptr = argv[1] + value_string.size();
  char *endptr = std::strchr(argv[1], '\0');

  float value_float(           strtof( argv[1], &endptr));
  double value_double(         strtod( argv[1], &endptr));
  long double value_longdouble(strtold(argv[1], &endptr));

  double value_float_next(std::nextafterf(value_double,      std::numeric_limits<float>::infinity()));
  double value_float_prev(std::nextafterf(value_float,      -std::numeric_limits<float>::infinity()));
  double value_double_next(std::nextafter(value_double,      std::numeric_limits<double>::infinity()));
  double value_double_prev(std::nextafter(value_double,     -std::numeric_limits<double>::infinity()));
  double value_longdouble_next(std::nextafter(value_double,  std::numeric_limits<long double>::infinity()));
  double value_longdouble_prev(std::nextafter(value_double, -std::numeric_limits<long double>::infinity()));

  std::cout << "float:  value                  = " << std::scientific << value_float                              << " ( " << std::fixed << value_float << ")" << std::endl;
  std::cout << "float:  nearest above          = " << std::scientific << value_float_next                         << " ( " << std::fixed << value_float_next << ")" << std::endl;
  std::cout << "float:  nearest below          = " << std::scientific << value_float_prev                         << " ( " << std::fixed << value_float_prev << ")" << std::endl;
  std::cout << "float:  minimum increment      = " << std::scientific << value_float_next - value_float           << " ( " << std::fixed << value_float_next - value_float << ")" << std::endl;
  std::cout << "float:  minimum decrement      = " << std::scientific << value_float - value_float_prev           << " ( " << std::fixed << value_float - value_float_prev << ")" << std::endl;
  std::cout << "double: value                  = " << std::scientific << value_double                             << " ( " << std::fixed << value_double << ")" << std::endl;
  std::cout << "double: nearest above          = " << std::scientific << value_double_next                        << " ( " << std::fixed << value_double_next << ")" << std::endl;
  std::cout << "double: nearest below          = " << std::scientific << value_double_prev                        << " ( " << std::fixed << value_double_prev << ")" << std::endl;
  std::cout << "double: minimum increment      = " << std::scientific << value_double_next - value_double         << " ( " << std::fixed << value_double_next - value_double << ")" << std::endl;
  std::cout << "double: minimum decrement      = " << std::scientific << value_double - value_double_prev         << " ( " << std::fixed << value_double - value_double_prev << ")" << std::endl;
  std::cout << "long double: value             = " << std::scientific << value_longdouble                         << " ( " << std::fixed << value_longdouble << ")" << std::endl;
  std::cout << "long double: nearest above     = " << std::scientific << value_longdouble_next                    << " ( " << std::fixed << value_longdouble_next << ")" << std::endl;
  std::cout << "long double: nearest below     = " << std::scientific << value_longdouble_prev                    << " ( " << std::fixed << value_longdouble_prev << ")" << std::endl;
  std::cout << "long double: minimum increment = " << std::scientific << value_longdouble_next - value_longdouble << " ( " << std::fixed << value_longdouble_next - value_longdouble << ")" << std::endl;
  std::cout << "long double: minimum decrement = " << std::scientific << value_longdouble - value_longdouble_prev << " ( " << std::fixed << value_longdouble - value_longdouble_prev << ")" << std::endl;

  return EXIT_SUCCESS;
}
